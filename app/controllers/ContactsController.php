<?php

  namespace App\Controllers;
  use Core\Controller;
  use Core\Session;
  use Core\Router;
  use App\Models\Contacts;
  use App\Models\Users;

  class ContactsController extends Controller {

    // Index Action to display a table of our contacts
    public function indexAction(){
      $user = Users::currentUser();
      $this->view->contacts = Contacts::findByUserId($user->id);
      $this->view->render('contacts/index');
    }

    //Controller Action for adding and editing contacts
    public function editAction($id='new'){
      $currentUser = Users::currentUser();
      if($id == 'new'){
        $contact = new Contacts();
      } else {
        $contact = Contacts::findByIdAndUserId((int)$id,$currentUser->id);
        if(!$contact) {
          Session::addMsg('warning',"You do not have permission to edit that contact.");
          Router::redirect('contacts');
        }
      }

      //handle form submission
      if($this->request->isPost()){
        $this->request->csrfCheck();
        $postedData = $this->request->get();
        $blackListedFields = ['id','deleted','user_id','created_at','updated_at'];
        $contact->assign($postedData,$blackListedFields);
        $contact->user_id = $currentUser->id;
        if($contact->save()){
          // set up a session message and redirect
          $msg = ($id == 'new')? "Contact Added!" : "Contact Updated!";
          Session::addMsg('success',$msg);
          Router::redirect('contacts');
        }
      }

      $this->view->contact = $contact;
      $this->view->formErrors = $contact->getErrorMessages();
      $this->view->header = ($id == 'new')? "Add A Contact" : "Edit Contact";
      $this->view->render('contacts/edit');
    }

    public function deleteAction(){
      $currentUser = Users::currentUser();
      $resp = ['success'=>false,'msg'=>'Delete Failed...'];
      if($this->request->isPost()){
        $contactId = $this->request->get('contact_id');
        $contact = Contacts::findByIdAndUserId($contactId,$currentUser->id);
        if($contact){
          $contact->delete();
          $resp = ['success'=>true,'msg'=>'Contact Deleted','id'=>$contactId];
        }
      }
      $this->jsonResponse($resp);
    }

  }
