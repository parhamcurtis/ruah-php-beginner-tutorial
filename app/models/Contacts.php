<?php
  namespace App\Models;
  use Core\Model;
  use Core\Validators\RequiredValidator;
  use Core\Validators\EmailValidator;
  use Core\Validators\UniqueValidator;

  class Contacts extends Model {

    // This tells the model what database table we are connecting to.
    protected static $_table = 'contacts';

    // If you want to soft delete the table rows declare the soft delete property
    protected static $_softDelete = true;

    // Create public properties for each column in the database table
    public $id, $created_at, $updated_at, $user_id, $fname, $lname, $email;
    public $phone, $street, $street2, $city, $state, $zip_code, $deleted = 0;

    // Validate Form Fields
    public function validator(){
      $this->runValidation(
        new RequiredValidator($this,['field'=>'fname','msg'=>'First Name is required.'])
      );

      $this->runValidation(
        new RequiredValidator($this,['field'=>'lname','msg'=>'Last Name is required.'])
      );

      $this->runValidation(
        new EmailValidator($this,['field'=>'email','msg'=>'You must provide a valid email address.'])
      );

      $this->runValidation(
        new UniqueValidator($this,['field'=>['email','user_id'],'msg'=>'That email is used by another contact.'])
      );
    }

    // Set timestamps before save
    public function beforeSave(){
      $this->timeStamps();
    }

    public static function findByUserId($user_id){
      $params = [
        'conditions' => "user_id = ?",
        'bind' => [$user_id],
        'order' => 'lname, fname'
      ];
      return self::find($params);
    }

    public function fullName(){
      return $this->fname . ' ' . $this->lname;
    }

    public static function findByIdAndUserId($id, $user_id){
      $params = [
        'conditions' => "id = ? AND user_id = ?",
        'bind' => [$id,$user_id]
      ];
      return self::findFirst($params);
    }

  }
